curl --location 'https://pallav-kumar.atlassian.net/rest/api/3/issue' \
--header 'content-type: application/json' \
--header 'Authorization: Basic cGFsbGF2ZGdyOEBnbWFpbC5jb206QVRBVFQzeEZmR0YwQzNWeWhfenZzX3dnZGphR2NOMzQ1N1g1T0ptRFFrNS01YjJXZjItSWt3cUlTSG9HN1RMY0doQlpEc3g0cjY0Tko0aTgxb3Jpd2NHT3lmNXlkMUtZbHUtbU0xdFpfc2g4QTJLRUdsNHdrcGF2dmk0amdEampYOUZwaUNoMG90ZEo4YU9FYUNTaUloUXVoWjAtcF9nWUNWcGFSb0dNQkxjM2NUS0ZTWV9uSmNjPTlENzNBRjlD' \
--header 'Cookie: atlassian.xsrf.token=70fad313-ab65-48bb-9bea-030d8f6c5e6e_f5c061eddebef0b7ccfcccf19cc708ca7ccada1c_lin' \
--data '{
        "fields":{
            "project":{
                "key":"M2M"
            },
            "summary": "TEST-SUMMARY-1",
            
            "issuetype":{
                "id":"10002",
                "name":"Bug"
            },
            "description": {

            "type": "doc",
            "version": 1,
            "content": [
                {
                "type": "paragraph",
                "content": [
                    {
                    "text": "This is the description.",
                    "type": "text"
                    }
                ]
                }
            ]
        },
    "labels": [
     "Label-From-1",
     "Label-TO-1"
     ]
        } 
}'